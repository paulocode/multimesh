# multimesh

A cross-platform Meshtastic® client based on Flutter

## Nightly builds
[![Nightly builds](https://app.bitrise.io/app/989c6c99-13a5-4938-9e00-d012acc48d87/status.svg?token=oYLjMn1LwvLKJufvosoNxA&branch=main)](https://app.bitrise.io/app/989c6c99-13a5-4938-9e00-d012acc48d87?commit_message=Nightly+build)


## Legal
Meshtastic® is a registered trademark of Meshtastic LLC. Meshtastic software components are released under various licenses, see GitHub for details. No warranty is provided - use at your own risk.
