// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'radio_config_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$radioConfigServiceHash() =>
    r'2eb41bb081b33f33f26edea9d761e0a329e66e9f';

/// See also [RadioConfigService].
@ProviderFor(RadioConfigService)
final radioConfigServiceProvider =
    NotifierProvider<RadioConfigService, RadioConfiguration>.internal(
  RadioConfigService.new,
  name: r'radioConfigServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$radioConfigServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$RadioConfigService = Notifier<RadioConfiguration>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
