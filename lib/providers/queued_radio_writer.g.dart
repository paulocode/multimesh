// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'queued_radio_writer.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$queuedRadioWriterHash() => r'505a4edf5fbcef0de65e83f7f835ec1df8a33b8e';

/// See also [queuedRadioWriter].
@ProviderFor(queuedRadioWriter)
final queuedRadioWriterProvider = Provider<QueuedRadioWriter>.internal(
  queuedRadioWriter,
  name: r'queuedRadioWriterProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$queuedRadioWriterHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef QueuedRadioWriterRef = ProviderRef<QueuedRadioWriter>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
